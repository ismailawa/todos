class Todo {
  final String text;
  final DateTime dueDate;
  final bool isDone;

  Todo({
    this.text,
    this.dueDate,
    this.isDone,
  });

  factory Todo.fromMap(Map<String, dynamic> json) => Todo(
      text: json["text"],
      dueDate: DateTime.parse(json["dueDate"]),
      isDone: json["isDone"]);

  Map<String, dynamic> toMap() =>
      {"text": text, "dueDate": dueDate.toIso8601String(), "isDone": isDone};
}
