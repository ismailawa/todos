import 'package:todo_application/models/todo.dart';

class TodoRepository {
  List<Todo> todos = [];

  Future<List<Todo>> getTodos() async {
    return Future<List<Todo>>.delayed(Duration(seconds: 5), () => todos);
    
  }

  Future<bool> addTodo(Todo todo) async {
    todos.add(todo);
    return Future.value(true);
  }
}
