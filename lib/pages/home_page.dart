import 'package:flutter/material.dart';
import 'package:todo_application/models/todo.dart';
import 'package:todo_application/repositories/todo_repository.dart';

class HomePage extends StatefulWidget {
  HomePage({
    Key key,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TodoRepository todoRepository = TodoRepository();
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextField(
                controller: _controller,
                decoration: InputDecoration(hintText: "Enter Todo List"),
              ),
              RaisedButton(
                color: Colors.black,
                child: Text(
                  "Add Todo",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () async {
                  setState(() {});
                  await todoRepository.addTodo(Todo(
                    text: _controller.text,
                    dueDate: DateTime.now(),
                    isDone: false,
                  ));
                  _controller.clear();
                },
              ),
              Expanded(
                child: Container(
                  height: 300,
                  child: FutureBuilder<List<Todo>>(
                      future: todoRepository.getTodos(),
                      builder: (context, AsyncSnapshot<List<Todo>> todos) {
                        if (todos.hasData) {
                          return ListView.builder(
                            itemCount: todos.data.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(10, 10, 10, 10),
                                child: Text("${todos.data[index].text}"),
                              );
                            },
                          );
                        } else {
                          return Container(
                            child: Center(child: Text("No Todos Found")),
                          );
                        }
                      }),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
